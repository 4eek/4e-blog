var html = require('choo/html')
var path = require('path')
var format = require('../components/format')

module.exports = view

function view (state, emit) {
  return html`
    <div class="x xw c12 p1 sm-mt4">
      <div class="p1 copy c6 sm-c12">
        ${format(state.page.text)} 
      </div>
      <div class="c6 sm-c12">
        ${state.page.image ? image() : ''}
      </div>
    </div>
  `

  function image () {
    return html` 
      <div class="p1">
        <div style="
          background-color: #eee;
          background-position: center;
          background-size: cover;
          background-repeat: no-repeat;
          background-image: url(${path.join(state.page.path, state.page.image)});
          padding-bottom: 100%;
        "></div>
        <div class="pt1 copy">
          ${state.page.caption ? format(state.page.caption) : ''}
        </div>
      </div>
    `
  }

}
